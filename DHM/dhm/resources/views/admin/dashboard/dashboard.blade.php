@extends('admin.layout.master')

@section('title','Dashboard')
@section('content')
    <div class="row">
        <div class="col-lg-8">
            <!-- Quick stats boxes -->
            <div class="row">
                <div class="col-lg-4">

                    <!-- Today's total meal -->
                    <div class="panel bg-teal-400">
                        <div class="panel-body">
                            <div class="heading-elements col-lg-5 no-padding-left">
                                <span class="heading-text badge bg-teal-800">Breakfast: 160</span>
                                <span class="heading-text badge bg-teal-800">Lunch: 90</span>
                                <span class="heading-text badge bg-teal-800">Supper: 41</span>
                            </div>
                            <h3 class="no-margin"><b>291 M.</b></h3>
                            <p class="no-margin">Today's total <br> meal</p>
                        </div>
                    </div>
                    <!-- /today's total meal -->

                </div>

                <div class="col-lg-4">

                    {{--Total cash in account--}}
                    <div class="panel bg-pink-400">
                        <div class="panel-body">
                            <h3 class="no-margin"><b>&#2547; 42560</b></h3>
                            <p class="mb-20">Total cash in account</p>
                        </div>
                    </div>
                    {{--/total cash in account--}}

                </div>

                <div class="col-lg-4">

                    {{--Total students paid money--}}
                    <div class="panel bg-danger-400">
                        <div class="panel-body ">
                            <div class="heading-elements col-lg-3 no-padding-left">
                                <span class="heading-text badge bg-teal-800" data-popup="tooltip" title data-container="body"
                                      data-original-title="Paid students no.">Std: 160</span>
                            </div>
                            <h3 class="no-margin"><b>&#2547; 42560880</b></h3>
                            <p class="no-margin">Paid Money<br> <b>this month</b></p>
                        </div>
                    </div>
                {{--/total students paid money--}}

                </div>
            </div>
            <!-- /quick stats boxes -->

            {{--Complain list--}}
            <div class="panel panel-flat">
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                        <tr>
                            <th style="width: 300px;">User</th>
                            <th>Complains&nbsp;Descriptions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="active border-double">
                            <td colspan="2">Total&nbsp;Complains</td>
                            <td class="text-right">
                                <span class="badge bg-blue">24</span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon">A</span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle
                                        Doney</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">[#1183] Workaround for OS X selects printing bug</span>
                                    <span class="display-block text-muted">Chrome fixed the bug several versions ago, thus rendering this...</span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="#" data-popup="tooltip" title data-container="body" data-original-title="Quick reply"><i
                                            class="icon-undo"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon">A</span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle
                                        Doney</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">[#1183] Workaround for OS X selects printing bug</span>
                                    <span class="display-block text-muted">Chrome fixed the bug several versions ago, thus rendering this...</span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="#" data-popup="tooltip" title data-container="body" data-original-title="Quick reply"><i
                                            class="icon-undo"></i></a>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div class="media-left media-middle">
                                    <a href="#"><img src="{{ asset('assets/images/placeholder.jpg') }}" class="img-circle img-xs"
                                                     alt=""></a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Chris
                                        Macintyre</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">[#1249] Vertically center carousel controls</span>
                                    <span class="display-block text-muted">Try any carousel control and reduce the screen width below...</span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="#" data-popup="tooltip" title data-container="body" data-original-title="Quick reply"><i
                                            class="icon-undo"></i></a>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-blue btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon">A</span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Robert
                                        Hauber</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">[#1254] Inaccurate small pagination height</span>
                                    <span class="display-block text-muted">The height of pagination elements is not consistent with...</span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="#" data-popup="tooltip" title data-container="body" data-original-title="Quick reply"><i
                                            class="icon-undo"></i></a>
                            </td>
                        </tr>

                        <tr>
                            {{--<td class="text-center">
                                <h6 class="no-margin">40 <small class="display-block text-size-small no-margin">hours</small></h6>
                            </td>--}}
                            <td>
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-warning-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon"></span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Dex
                                        Sponheim</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">[#1184] Round grid column gutter operations</span>
                                    <span class="display-block text-muted">Left rounds up, right rounds down. should keep everything...</span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="#" data-popup="tooltip" title data-container="body" data-original-title="Quick reply"><i
                                            class="icon-undo"></i></a>
                            </td>
                            {{--<td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                            <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="icon-checkmark3 text-success"></i> Resolve issue</a></li>
                                            <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>--}}
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table text-nowrap table-hover">
                        <tbody>
                        <tr class="active border-double">
                            <td colspan="2">This&nbsp;month&nbsp;bazar&nbsp;Details&nbsp;-
                                <span>19/01/2018</span></td>
                            <td class="text-right">
                                <span class="badge bg-blue">&#2547; 200686</span>
                            </td>
                        </tr>

                        <tr>
                            <th style="width: 150px;">Manager&nbsp;Name</th>
                            <th>Date</th>
                            <th data-popup="tooltip" data-placement="right" title data-container="body"
                                data-original-title="Total Cost">T.Cost
                            </th>
                        </tr>

                        <tr>
                            <td style="width: 150px">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon">A</span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle
                                        Doney</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">18/02/2018</span>
                                    <span class="display-block text-muted">আলু, টমেটো, কুমড়া, বরবটি...</span>
                                </a>
                            </td>
                            <td class="text-left">
                                &#2547; 320993
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon">A</span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle
                                        Doney</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">18/02/2018</span>
                                    <span class="display-block text-muted">আলু, টমেটো, কুমড়া, বরবটি...</span>
                                </a>
                            </td>
                            <td class="text-left">
                                &#2547; 320993
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon">A</span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle
                                        Doney</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">18/02/2018</span>
                                    <span class="display-block text-muted">আলু, টমেটো, কুমড়া, বরবটি...</span>
                                </a>
                            </td>
                            <td class="text-left">
                                &#2547; 320993
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon">A</span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle
                                        Doney</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">18/02/2018</span>
                                    <span class="display-block text-muted">আলু, টমেটো, কুমড়া, বরবটি...</span>
                                </a>
                            </td>
                            <td class="text-left">
                                &#2547; 320993
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- /complain list--}}

        </div>

        <div class="col-lg-4">

            <!-- All messages -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">My messages</h6>
                    <div class="heading-elements">
                        <span class="heading-text"><i class="icon-history text-warning position-left"></i> Jul 7, 10:30</span>
                        <span class="label bg-success heading-text">Online</span>
                    </div>
                </div>

                <!-- Numbers -->
                <div class="container-fluid">
                    <div class="row text-center">
                        <div class="col-md-4">
                            <div class="content-group">
                                <h6 class="text-semibold no-margin"><i class="icon-clipboard3 position-left text-slate"></i> 2,345
                                </h6>
                                <span class="text-muted text-size-small">this week</span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content-group">
                                <h6 class="text-semibold no-margin"><i class="icon-calendar3 position-left text-slate"></i> 3,568
                                </h6>
                                <span class="text-muted text-size-small">this month</span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content-group">
                                <h6 class="text-semibold no-margin"><i class="icon-comments position-left text-slate"></i> 32,693
                                </h6>
                                <span class="text-muted text-size-small">all messages</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /numbers -->


                <!-- Area chart -->
                <div id="messages-stats"></div>
                <!-- /area chart -->


                <!-- Tabs -->
                <ul class="nav nav-lg nav-tabs nav-justified no-margin no-border-radius bg-indigo-400 border-top border-top-indigo-300">
                    <li class="active">
                        <a href="#messages" class="text-size-small text-uppercase" data-toggle="tab">
                            Tuesday
                        </a>
                    </li>
                </ul>
                <!-- /tabs -->


                <!-- Tabs content -->
                <div class="tab-content">
                    <div class="tab-pane active fade in has-padding" id="messages">
                        <ul class="media-list">
                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                    <span class="badge bg-danger-400 media-badge">8</span>
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        James Alexander
                                        <span class="media-annotation pull-right">14:58</span>
                                    </a>

                                    <span class="display-block text-muted">The constitutionally inventoried precariously...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                    <span class="badge bg-danger-400 media-badge">6</span>
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Margo Baker
                                        <span class="media-annotation pull-right">12:16</span>
                                    </a>

                                    <span class="display-block text-muted">Pinched a well more moral chose goodness...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Jeremy Victorino
                                        <span class="media-annotation pull-right">09:48</span>
                                    </a>

                                    <span class="display-block text-muted">Pert thickly mischievous clung frowned well...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Beatrix Diaz
                                        <span class="media-annotation pull-right">05:54</span>
                                    </a>

                                    <span class="display-block text-muted">Nightingale taped hello bucolic fussily cardinal...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Richard Vango
                                        <span class="media-annotation pull-right">01:43</span>
                                    </a>

                                    <span class="display-block text-muted">Amidst roadrunner distantly pompously where...</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /tabs content -->

            </div>
            <!-- /all messages -->
        </div>
        <div class="col-lg-4">

            <!-- Disable member list -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Disable&nbsp;List</h6>
                    <div class="heading-elements">
                        <span class="heading-text text-danger" data-popup="tooltip" data-container="body" title
                              data-original-title="Total" data-placement="left"><i
                                    class="icon-lock text-danger position-left"></i> 6</span>
                    </div>
                </div>

                <!-- Numbers -->
                <div class="container-fluid">
                    <div class="row text-center">
                        <div class="col-md-6">
                            <div class="content-group">
                                <h6 class="text-semibold no-margin"><i class="icon-user-lock position-left text-danger"></i> 2,345
                                </h6>
                                <span class="text-muted text-size-small">Members</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="content-group">
                                <h6 class="text-semibold no-margin"><i class="icon-user-lock position-left text-danger  "></i>
                                    32,693
                                </h6>
                                <span class="text-muted text-size-small">Managers</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /numbers -->


                <!-- Area chart -->
                <div id="messages-stats"></div>
                <!-- /area chart -->


                <!-- Tabs -->
                <ul class="nav nav-lg nav-tabs nav-justified no-margin no-border-radius bg-indigo-400 border-top border-top-indigo-300">
                    <li class="active">
                        <a href="#disable-member" class="text-size-small text-uppercase" data-toggle="tab">
                            Member
                        </a>
                    </li>

                    <li>
                        <a href="#disable-manager" class="text-size-small text-uppercase" data-toggle="tab">
                            Manager
                        </a>
                    </li>
                </ul>
                <!-- /tabs -->


                <!-- Tabs content -->
                <div class="tab-content">
                    <div class="tab-pane active fade in has-padding" id="disable-member">
                        <ul class="media-list">
                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                    <span class="badge bg-danger-400 media-badge">8</span>
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        James Alexander
                                        <span class="media-annotation pull-right">14:58</span>
                                    </a>

                                    <span class="display-block text-muted">The constitutionally inventoried precariously...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                    <span class="badge bg-danger-400 media-badge">6</span>
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Margo Baker
                                        <span class="media-annotation pull-right">12:16</span>
                                    </a>

                                    <span class="display-block text-muted">Pinched a well more moral chose goodness...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Jeremy Victorino
                                        <span class="media-annotation pull-right">09:48</span>
                                    </a>

                                    <span class="display-block text-muted">Pert thickly mischievous clung frowned well...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Beatrix Diaz
                                        <span class="media-annotation pull-right">05:54</span>
                                    </a>

                                    <span class="display-block text-muted">Nightingale taped hello bucolic fussily cardinal...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-xs" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Richard Vango
                                        <span class="media-annotation pull-right">01:43</span>
                                    </a>

                                    <span class="display-block text-muted">Amidst roadrunner distantly pompously where...</span>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-pane fade has-padding" id="disable-manager">
                        <ul class="media-list">
                            <li class="media">
                                <div class="media-left">
                                    <img src="{{ asset('assets/images/placeholder.jpg') }}" class="img-circle img-sm" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Isak Temes
                                        <span class="media-annotation pull-right">Tue, 19:58</span>
                                    </a>

                                    <span class="display-block text-muted">Reasonable palpably rankly expressly grimy...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="{{ asset('assets/images/placeholder.jpg') }}" class="img-circle img-sm" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Vittorio Cosgrove
                                        <span class="media-annotation pull-right">Tue, 16:35</span>
                                    </a>

                                    <span class="display-block text-muted">Arguably therefore more unexplainable fumed...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Hilary Talaugon
                                        <span class="media-annotation pull-right">Tue, 12:16</span>
                                    </a>

                                    <span class="display-block text-muted">Nicely unlike porpoise a kookaburra past more...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Bobbie Seber
                                        <span class="media-annotation pull-right">Tue, 09:20</span>
                                    </a>

                                    <span class="display-block text-muted">Before visual vigilantly fortuitous tortoise...</span>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                                </div>

                                <div class="media-body">
                                    <a href="#">
                                        Walther Laws
                                        <span class="media-annotation pull-right">Tue, 03:29</span>
                                    </a>

                                    <span class="display-block text-muted">Far affecting more leered unerringly dishonest...</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /tabs content -->
            </div>
            <!-- /my messages -->
        </div>

        <div class="col-lg-12">
            <!-- All notices -->
            <div class="panel panel-flat">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th colspan="4">
                                <h3 class="panel-title">All Notices <span class="badge bg-warning-400">523</span></h3>
                            </th>
                        </tr>
                        <tr class="active">
                            <th>Sl</th>
                            <th>Given By</th>
                            <th>Notices</th>
                            <th>Date</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="col-md-1">
                                <label class="checkbox-inline">
                                    <div>
                                        <input type="checkbox" class="border-primary" value="">
                                    </div>
                                    1
                                </label>
                            </td>
                            <td class="col-md-3">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon">A</span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle
                                        Doney</a>
                                    <div class="text-muted text-size-small"><span
                                                class="status-mark border-blue position-left"></span> Active
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="text-default display-inline-block">
                                    <span class="text-semibold">Title</span>
                                    <span class="display-block text-muted">আলু, টমেটো, কুমড়া, বরবটি...</span>
                                </a>
                            </td>
                            <td class="col-md-2">
                                <a href="#" class="text-default">
                                    <span>18/01/2018</span>
                                </a>
                            </td>
                            <td class="col-md-2">
                                <div class="display-inline-block">
                                    <a href="#"><i class="icon-pencil7 text-indigo"></i></a>
                                    <a href="#"><i class="icon-trash text-danger"></i></a>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /all notices -->


        </div>
    </div>

@endsection